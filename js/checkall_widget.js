(function ($, Drupal) {
  Drupal.behaviors.checkall_widget = {
    attach(context) {
      $('.checkall-widget-btn').on('click', function (e) {
        e.preventDefault();
        $(this)
          .parents('.field--widget-checkall-widget-options-buttons')
          .find('input')
          .prop('checked', typeof $(this).data('checkAll') !== 'undefined');
      });
    },
  };
})(jQuery, Drupal);
