<?php

namespace Drupal\checkall_widget\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsButtonsWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Plugin implementation of the 'options_buttons' widget.
 *
 * @FieldWidget(
 *   id = "checkall_widget_options_buttons",
 *   label = @Translation("Checkall Widget : Check all boxes/radio buttons"),
 *   description = @Translation("Enable to check/uncheck all buttons."),
 *   field_types = {
 *     "boolean",
 *     "entity_reference",
 *     "list_integer",
 *     "list_float",
 *     "list_string",
 *   },
 *   multiple_values = TRUE
 * )
 */
class CheckallOptionsWidget extends OptionsButtonsWidget {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $element['all_wrapper'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['checkall-widget']],
      '#weight' => 0,
    ];
    $element['all_wrapper']['check_all'] = [
      '#type' => 'html_tag',
      '#tag' => 'button',
      '#value' => $this->t('Check all'),
      '#default_value' => FALSE,
      '#attributes' => [
        'class' => [
          'checkall-widget-btn',
          'button--small',
        ],
        'data-check-all' => TRUE,
      ],
    ];
    $element['all_wrapper']['uncheck_all'] = [
      '#type' => 'html_tag',
      '#tag' => 'button',
      '#value' => $this->t('Uncheck all'),
      '#default_value' => FALSE,
      '#attributes' => [
        'class' => [
          'checkall-widget-btn',
          'button--small',
        ],
        'data-check-all' => FALSE,
      ],
    ];

    $element['#attached']['library'][] = 'checkall_widget/checkall_widget';

    return $element;
  }

}
