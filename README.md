# checkall_widget widget

## Summary

Allows you to add to the checkboxes the possibility of checking / unchecking all the boxes.

## OptionsButtonsWidget

This widget is available for type fields:
* boolean
* entity_reference
* list_integer
* list_float
* list_string
